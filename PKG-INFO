Metadata-Version: 2.1
Name: test_server
Version: 0.0.43
Summary: Server for testing HTTP clients
Author-email: Gregory Petukhov <lorien@lorien.name>
License: The MIT License (MIT)
        
        Copyright (c) 2015-2023, Gregory Petukhov
        
        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:
        
        The above copyright notice and this permission notice shall be included in
        all copies or substantial portions of the Software.
        
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
        THE SOFTWARE.
        
Project-URL: homepage, http://github.com/lorien/test_server
Keywords: test,testing,server,http server
Classifier: Typing :: Typed
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.7
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: License :: OSI Approved :: MIT License
Classifier: Development Status :: 4 - Beta
Classifier: Intended Audience :: Developers
Classifier: Operating System :: OS Independent
Classifier: Topic :: Software Development :: Libraries :: Python Modules
Classifier: Topic :: Internet :: WWW/HTTP
Requires-Python: >=3.8
Description-Content-Type: text/markdown
License-File: LICENSE

# Documentation for test_server package

[![Test Status](https://github.com/lorien/test_server/actions/workflows/test.yml/badge.svg)](https://github.com/lorien/test_server/actions/workflows/test.yml)
[![Code Quality](https://github.com/lorien/test_server/actions/workflows/check.yml/badge.svg)](https://github.com/lorien/test_server/actions/workflows/test.yml)
[![Type Check](https://github.com/lorien/test_server/actions/workflows/mypy.yml/badge.svg)](https://github.com/lorien/test_server/actions/workflows/mypy.yml)
[![Test Coverage Status](https://coveralls.io/repos/github/lorien/test_server/badge.svg)](https://coveralls.io/github/lorien/test_server)
[![Documentation Status](https://readthedocs.org/projects/test_server/badge/?version=latest)](http://user-agent.readthedocs.org)

Simple HTTP Server for testing HTTP clients.


## Installation

Run `pip install -U test_server`


## Usage Example

```python
from unittest import TestCase
import unittest
from urllib.request import urlopen

from test_server import TestServer, Response, HttpHeaderStorage


class UrllibTestCase(TestCase):
   @classmethod
   def setUpClass(cls):
       cls.server = TestServer()
       cls.server.start()

   @classmethod
   def tearDownClass(cls):
       cls.server.stop()

   def setUp(self):
       self.server.reset()

   def test_get(self):
       self.server.add_response(
           Response(
               data=b"hello",
               headers={"foo": "bar"},
           )
       )
       self.server.add_response(Response(data=b"zzz"))
       url = self.server.get_url()
       info = urlopen(url)
       self.assertEqual(b"hello", info.read())
       self.assertEqual("bar", info.headers["foo"])
       info = urlopen(url)
       self.assertEqual(b"zzz", info.read())
       self.assertTrue("bar" not in info.headers)


   unittest.main()
```
